package com.lkakulia.lecture_13

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val items = mutableListOf<ItemModel>()
    lateinit private var listAvatars: Array<ItemModel>
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        //initialize a list of male && female avatars from which we will randomly choose later
        listAvatars = arrayOf(
            ItemModel(R.mipmap.ic_avatar_male, "Male Avatar"),
            ItemModel(R.mipmap.ic_avatar_female, "Female Avatar")
        )


        adapter = RecyclerViewAdapter(items, object: RemoveItemInterface {
            override fun onClick(position: Int) {
                items.removeAt(position)
                adapter.notifyItemRemoved(position)
            }
        })

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }

    fun addItemOnClick(view: View) {
        //choose a random index from listAvatars
        val randomAvatarIndex = listAvatars.indices.random()

        //add corresponding avatar to the items' list
        items.add(0, listAvatars[randomAvatarIndex])
        adapter.notifyItemInserted(0)
        recyclerView.scrollToPosition(0)
    }
}
