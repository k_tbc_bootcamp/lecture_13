package com.lkakulia.lecture_13

interface RemoveItemInterface {
    fun onClick(position: Int)
}